from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def calculate():
    profit = None
    if request.method == 'POST':
        try:
            buy_price = float(request.form['buy_price'])
            sell_price = float(request.form['sell_price'])
            amount = float(request.form['amount'])
            profit = (sell_price - buy_price) * amount
        except ValueError:
            profit = "Please enter valid numbers."
    return render_template('index.html', profit=profit)

if __name__ == '__main__':
    app.run(debug=False)
