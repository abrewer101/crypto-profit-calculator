# GUI Crypto Profit Calculator

## Introduction
The GUI Crypto Profit Calculator is a simple Python application with a graphical user interface designed to help users calculate the potential profit or loss from buying and selling cryptocurrency. It allows users to input the buy price, sell price, and amount of cryptocurrency through a user-friendly interface and instantly calculates the profit from these transactions.

## Features
- **Simple Graphical Interface:** Easy to use graphical interface for inputting transaction details.
- **Immediate Calculations:** Instantly calculates and displays the profit or loss from your cryptocurrency transactions.
- **Flexibility:** Can be used for any cryptocurrency, allowing inputs for buy price, sell price, and the amount.

## Requirements
- Python 3.x
- Tkinter (usually comes with Python)

## Installation

### Clone the Repository (Optional)
If you have git installed, you can clone the repository to get the application files. git clone <repository-url>
Replace `<repository-url>` with the URL of the GitLab repository where the application is hosted.

### Download the Script
Alternatively, you can simply download the `gui_crypto_profit_calculator.py` file directly from the GitLab repository to your local machine.

## Running the Application
To run the GUI Crypto Profit Calculator, follow these steps:

1. Ensure Python 3.x is installed on your system.
2. Navigate to the directory containing `gui_crypto_profit_calculator.py`.
3. Run the script by typing the following command in your terminal or command prompt: python gui_crypto_profit_calculator.py

Use `python3` instead of `python` if your system requires it to run Python 3.x applications.

## Usage
After starting the application, you will see the graphical interface. Follow these steps to calculate your profit:

1. Enter the buy price per coin in the designated field.
2. Enter the sell price per coin in the next field.
3. Enter the amount of cryptocurrency you're trading.
4. Click the "Calculate" button to see your profit displayed in the interface.

## Contributing
Contributions to the GUI Crypto Profit Calculator are welcome. If you have suggestions for improvements or find any bugs, please feel free to fork the repository, make your changes, and submit a pull request.
